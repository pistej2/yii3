<?php

return [
    'app.id' => 'my-admin',
    'app.name' => 'MyAdmin',

    'adminEmail' => 'admin@example.com',

    'db.dsn'        => "mysql:host=localhost;dbname=yii3;charset=utf8",
    'db.username'   => 'root',
    'db.password'   => '',

    'favicon.ico' => '@yii/app/../public/favicon.ico',
    'debug.allowedIPs' => ['127.0.0.1'],
    'debug.enabled' => true,
//    'debug.logTarget' => 'logger',
];
